# Order Project

- Language:       Node.js
- Created by:     Annabelle Yeung
- Last updated:   16 Jan 2020

## Usage

1. Set up backend with your own Google Map API key (instructions in Configuration section)
1. Run start.sh to start backend at port 8080. First build may take some time.
2. In case you want to access MySQL database, the port is 3307
3. Run stop.sh to stop backend

## Configuration

1. To set up Google Map API key, go to /app/app.env and update value of GOOGLE_API_KEY
2. To change backend serving port, go to .env and update value of NODE_EXPOSE_PORT
3. To change database serving port, go to .env and update value of MYSQL_EXPOSE_PORT
4. MySQL database is using permanent storage. Remove the volume mounting in docker-compose.yml if you want to reset database when containers are terminated (i.e. remove 'volumes' property in yml file)
5. npm test will be run during docker-compose build. To remove this step, remove the line 'RUN npm test' in /app/Dockerfile

## Assumption

The following assumptions are made on the function requirement:

1. Distance is the driving distance returned by Google Map API. Origin-destination pair which is impossible to be completed by only driving will have distance = 0 returned
2. GET /orders must provide query parameter page and limit
3. GET /orders can take page number larger than maximum page number, and in such case the response will be empty array
4. Orders returned are sorted by created date in ascending order, i.e. the newest order will be at the bottom of order list
5. UUID is case insensitive. Therefore in PATCH /orders, ID can be in either uppercase or lowercase