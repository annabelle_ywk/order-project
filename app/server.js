const bodyParser = require('body-parser');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const express = require('express');
const cors = require('cors');

const config = require('./config');
const constants = require('./constants');
const routes = require('./routes');
const LogService = require('./util/LogService');
const logService = new LogService({ logger: { level: config.LOG_LEVEL }});

global.logger = logService.logger;
global.helper = require('./util/helper');
global.dbconn = require('./util/dbconn');

const app = express();

//Skip cert verification when connecting to HTTPS server with self-signed cert
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

//Apply general middlewares
app.use(
  compression(),
  bodyParser.urlencoded({ extended: true }),
  cookieParser(),
  cors({
    origin: config.CORS_ALLOW_ORIGIN,
    methods: config.CORS_ALLOW_METHODS,
    allowedHeaders: config.CORS_ALLOW_HEADERS,
    credentials: true,
    optionsSuccessStatus: 200
  }),
  logService.middlewares.bindNamespace,
  logService.middlewares.bindRequestId
);

//Handle invalid request body
app.use(function(req, res, next) {
  bodyParser.json()(req, res, err => {
    if (err) {
      return res.status(constants.HTTP_CODE_BAD_REQUEST).json({ error: 'Request being invalid json' });
    }
    next();
  })
});

//Add security headers
app.use(function(req, res, next) {
  res.setHeader('Strict-Transport-Security', 'max-age=31536000; includeSubDomains');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  res.setHeader('X-Frame-Options', 'SAMEORIGIN');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  next();
});

//Handle the request by the defined routes
app.use('/', function(req, res, next) {
  helper.logRequest(req);
  next();
}, routes);

//Throw 404 if no route is matched
app.use(function(req, res) {
  helper.sendResponse(res)(constants.HTTP_CODE_NOT_FOUND, { message: 'The requested URL cannot be found' });
});

//Catch unhandled error
process.on('uncaughtException', function (err) {
  logger.error('Uncaught Exception', { error: err });
});

app.listen(config.PORT, function() {
  logger.info('Application is listening at port ' + config.PORT);
});

module.exports = app;
