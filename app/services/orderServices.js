const axios = require('axios');
const config = require('../config');
const constants = require('../constants');

const responseAttributes = ['id', 'distance', 'status'];

module.exports = {
  /* 
    Description: Retrieve all orders from database
    Parameters:
      page      - int; null if no pagination
      limit     - int; null if no pagination
      callback  - callback function; should consume return parameters
    Return:
      Pass the following to callback function:
      success - boolean; true if SQL query is executed successfully
      result  - array[<order>] if success = true; err if success = false
  */
  getAllOrders: function(page, limit, callback) {
    let searchCriteria = { attributes: responseAttributes };
    if (page != null && limit != null) {
      searchCriteria.offset = Number(page - 1) * Number(limit);
      searchCriteria.limit = Number(limit);
    }
    dbconn.Order
    .findAll(searchCriteria)
    .then(orders => {
      callback(true, orders);
    })
    .catch(err => {
      callback(false, err);
    });;
  },
  /* 
    Description: Retrieve order of given ID
    Parameters:
      id        - string; should be UUID
      callback  - callback function; should consume return parameters
    Return:
      Pass the following to callback function:
      success - boolean; true if SQL query is executed successfully
      result  - <order> if order of given ID exists; null if order does not exist; err if success = false
  */
  getOrderById: function(id, callback) {
    dbconn.Order
    .findAll({ attributes: responseAttributes, where: { id: id } })
    .then(orders => {
      callback(true, orders.length > 0? orders[0] : null);
    })
    .catch(err => {
      callback(false, err);
    });;
  },
  /* 
    Description: Create order of given distance
    Parameters:
      distance  - int; in meters
      callback  - callback function; should consume return parameters
    Return:
      Pass the following to callback function:
      success - boolean; true if SQL query is executed successfully
      result  - int; order ID of record created; err if success = false
  */
  createOrder: function(distance, callback) {
    dbconn.Order
    .create({ distance: distance, status: constants.ORDER_STATUS.UNASSIGNED })
    .then(result => {
      if (!helper.isSqlInsertSuccess(result, 'id')) {
        callback(false, null);
        return;
      }
      callback(true, result.id);
    })
    .catch(err => {
      callback(false, err);
    });
  },
  /* 
    Description: Update order of given ID status to TAKEN
    Parameters:
      id        - string; should be UUID
      callback  - callback function; should consume return parameters
    Return:
      Pass the following to callback function:
      success - boolean; true if order status is updated to TAKEN; false if cannot find UNASSIGNED order of given ID, or SQL query cannot be executed
      result  - null if success = false due to unable to find UNASSIGNED order of given ID; err if success = false due to SQL error
  */
  takeOrder: function(id, callback) {
    dbconn.Order
    .update(
      { status: constants.ORDER_STATUS.TAKEN },
      { where: { id: id, status: constants.ORDER_STATUS.UNASSIGNED } }
    )
    .then(result => {
      if (!helper.isSqlUpdateSuccess(result, 1)) {
        callback(false, null);
        return;
      }
      callback(true);
    })
    .catch(err => {
      callback(false, err);
    });
  },
  /* 
    Description: Retrieve order of given ID
    Parameters:
      originLat   - string; should be origin's latitude
      originLong  - string; should be origin's longtitude
      destLat     - string; should be destination's latitude
      destLong    - string; should be destination's longtitude
      callback    - callback function; should consume return parameters
    Return:
      Pass the following to callback function:
      distance    - int; in meters; null if unable to obtain valid distance from Google Map API
  */
  obtainDistance: function(originLat, originLong, destLat, destLong, callback) {
    const request = {
      method: 'GET',
      url: config.GOOGLE_MAP_API_URL,
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        key: config.GOOGLE_API_KEY,
        mode: 'driving',
        units: 'metric',
        origins: originLat + ',' + originLong,
        destinations: destLat + ',' + destLong
      },
      timeout: 10 * 1000
    }
    helper.logOutboundRequest(request);

    axios(request)
    .then(response => {
      if (response == null || response.data == null) {
        logger.error('Empty response from Google Map API');
        callback(null);
        return;
      }
      helper.logOutboundResponseSuccess(response);
      if (!helper.isArray(response.data.rows, 1, null) || !helper.isArray(response.data.rows[0].elements, 1, null)) {
        var errorMessage = 'Unable to interpret response from Google Map API';
        if (response.data.error_message != null && response.data.error_message.length > 0) {
          errorMessage = response.data.error_message;
        } else if (response.data.status != null && response.data.status.length > 0) {
          errorMessage = 'Status is ' + response.data.status;
        }
        logger.error('Google MAP API error - ' + errorMessage);
        callback(null);
        return;
      }
      const data = response.data.rows[0].elements[0];
      if (data == null || data.distance == null || data.distance.value == null) {
        if (data.status != constants.GOOGLE_MAP_API_STATUS.DISTANCE.ZERO_RESULTS) {
          logger.error('Unable to retrieve distance from response of Google Map API');
          callback(null);
          return;
        }
        callback(0);
        return;
      }
      const distance = Number(data.distance.value);
      callback(isNaN(distance)? null : distance);
    })
    .catch(error => {
      helper.logOutboundResponseError(error);
      callback(null);
    });
  }
}
