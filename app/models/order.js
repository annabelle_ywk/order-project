const constants = require('../constants');

module.exports = (Sequelize) => {
  return {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    distance: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
      allowNull: false
    },
    status: {
      type: Sequelize.ENUM(constants.ORDER_STATUS.UNASSIGNED, constants.ORDER_STATUS.TAKEN),
      defaultValue: constants.ORDER_STATUS.UNASSIGNED,
      allowNull: false
    }
  };
};
