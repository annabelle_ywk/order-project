#!/bin/sh
NODE_ENV=localhost \
PORT=8080 \
TZ=Asia/Hong_Kong \
DB_ENDPOINT=localhost \
DB_PORT=3306 \
DB_USERNAME=root \
DB_PASSWORD= \
DB_SCHEMA=ordering \
GOOGLE_API_KEY=put_your_key_here \
GOOGLE_MAP_API_URL=https://maps.googleapis.com/maps/api/distancematrix/json \
npm start