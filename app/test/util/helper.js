const async = require('async');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const nock = require('nock');
const config = require('../../config');
const constants = require('../../constants');
const orderServices = require('../../services/orderServices');
const googleResponse = require('../test_data/google-response');

chai.use(chaiHttp);

describe('--------------- util/helper ---------------', () => {

  describe('isArray', () => {
    describe('obj = null', () => {
      it('It should return false', (done) => {
        const result = helper.isArray(null, 1, 1);
        result.should.be.eql(false);
        done();
      });
    });
    describe('obj is not array', () => {
      it('It should return false', (done) => {
        const result = helper.isArray({key: 'value'}, 1, 1);
        result.should.be.eql(false);
        done();
      });
    });
    describe('obj is array with no length boundary', () => {
      it('It should return true', (done) => {
        const result = helper.isArray(['1','2'], null, null);
        result.should.be.eql(true);
        done();
      });
    });
    describe('obj is array with length less than min', () => {
      it('It should return false', (done) => {
        const result = helper.isArray(['1','2'], 3, null);
        result.should.be.eql(false);
        done();
      });
    });
    describe('obj is array with length more than max', () => {
      it('It should return false', (done) => {
        const result = helper.isArray(['1','2'], null, 1);
        result.should.be.eql(false);
        done();
      });
    });
    describe('obj is array with length within boundary', () => {
      it('It should return false', (done) => {
        const result = helper.isArray(['1','2'], 2, 2);
        result.should.be.eql(true);
        done();
      });
    });
  });

  describe('isDouble', () => {
    describe('str = null', () => {
      it('It should return false', (done) => {
        const result = helper.isDouble(null, null, null, null, null);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str = Number(1)', () => {
      it('It should return false', (done) => {
        const result = helper.isDouble(1, null, null, null, null);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str is empty string', () => {
      it('It should return false', (done) => {
        const result = helper.isDouble('', null, null, null, null);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str does not match double regex', () => {
      it('It should return false', (done) => {
        var result = helper.isDouble('1.2.3', null, null, null, null);
        result.should.be.eql(false);

        result = helper.isDouble('.', null, null, null, null);
        result.should.be.eql(false);

        result = helper.isDouble('--1.2', null, null, null, null);
        result.should.be.eql(false);

        done();
      });
    });
    describe('str matches double regex, no boundary check', () => {
      it('It should return false', (done) => {
        var result = helper.isDouble('0.001', null, null, null, null);
        result.should.be.eql(true);

        result = helper.isDouble('+1.0', null, null, null, null);
        result.should.be.eql(true);

        result = helper.isDouble('-.3', null, null, null, null);
        result.should.be.eql(true);

        done();
      });
    });
    describe('str matches double regex, less than min (excluded)', () => {
      it('It should return false', (done) => {
        const result = helper.isDouble('1.2', '1.2', false, null, null);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str matches double regex, less than min (included)', () => {
      it('It should return false', (done) => {
        const result = helper.isDouble('1.2', '1.21', true, null, null);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str matches double regex, greater than min (excluded)', () => {
      it('It should return true', (done) => {
        const result = helper.isDouble('1.2', '1.19', false, null, null);
        result.should.be.eql(true);
        done();
      });
    });
    describe('str matches double regex, greater than min (included)', () => {
      it('It should return true', (done) => {
        const result = helper.isDouble('1.2', '1.2', true, null, null);
        result.should.be.eql(true);
        done();
      });
    });
    describe('str matches double regex, greater than max (excluded)', () => {
      it('It should return false', (done) => {
        const result = helper.isDouble('1.2', null, null, '1.2', false);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str matches double regex, greater than max (included)', () => {
      it('It should return false', (done) => {
        const result = helper.isDouble('1.2', null, null, '1.19', true);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str matches double regex, less than max (excluded)', () => {
      it('It should return true', (done) => {
        const result = helper.isDouble('1.2', null, null, '1.21', false);
        result.should.be.eql(true);
        done();
      });
    });
    describe('str matches double regex, less than max (included)', () => {
      it('It should return true', (done) => {
        const result = helper.isDouble('1.2', null, null, '1.2', true);
        result.should.be.eql(true);
        done();
      });
    });
    describe('str matches double regex, within boundary', () => {
      it('It should return true', (done) => {
        const result = helper.isDouble('1.2', '1.2', true, '1.2', true);
        result.should.be.eql(true);
        done();
      });
    });
  });

  describe('isPositiveInteger', () => {
    describe('str = null', () => {
      it('It should return false', (done) => {
        const result = helper.isPositiveInteger(null, null, null);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str is empty string', () => {
      it('It should return false', (done) => {
        const result = helper.isPositiveInteger('', null, null);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str is invalid positive', () => {
      it('It should return false', (done) => {
        var result = helper.isPositiveInteger('-2', null, null);
        result.should.be.eql(false);

        result = helper.isPositiveInteger('0', null, null);
        result.should.be.eql(false);

        result = helper.isPositiveInteger('+1', null, null);
        result.should.be.eql(false);

        done();
      });
    });
    describe('str is valid positive, no boundary check', () => {
      it('It should return true', (done) => {
        const result = helper.isPositiveInteger('1', null, null);
        result.should.be.eql(true);
        done();
      });
    });
    describe('str is valid positive, less than min', () => {
      it('It should return false', (done) => {
        const result = helper.isPositiveInteger('1', 2, null);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str is valid positive, greater than or equal to min', () => {
      it('It should return true', (done) => {
        const result = helper.isPositiveInteger('1', 1, null);
        result.should.be.eql(true);
        done();
      });
    });
    describe('str is valid positive, greater than max', () => {
      it('It should return false', (done) => {
        const result = helper.isPositiveInteger('2', null, 1);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str is valid positive, less than or equal to max', () => {
      it('It should return true', (done) => {
        const result = helper.isPositiveInteger('2', null, 2);
        result.should.be.eql(true);
        done();
      });
    });
    describe('str is valid positive, within boundary', () => {
      it('It should return true', (done) => {
        const result = helper.isPositiveInteger('1', 1, 1);
        result.should.be.eql(true);
        done();
      });
    });
  });

  describe('isUUID', () => {
    describe('str = null', () => {
      it('It should return false', (done) => {
        const result = helper.isUUID(null);
        result.should.be.eql(false);
        done();
      });
    });
    describe('str is empty string', () => {
      it('It should return false', (done) => {
        const result = helper.isUUID('');
        result.should.be.eql(false);
        done();
      });
    });
    describe('str does not match UUID regex', () => {
      it('It should return false', (done) => {
        const result = helper.isUUID('1234');
        result.should.be.eql(false);
        done();
      });
    });
    describe('str matches UUID regex', () => {
      it('It should return true', (done) => {
        const result = helper.isUUID('12345678-abcd-1234-abcd-1234567890ab');
        result.should.be.eql(true);
        done();
      });
    });
  });

  describe('isSqlUpdateSuccess', () => {
    describe('updated number of rows = 0', () => {
      it('It should return false', (done) => {
        const result = helper.isSqlUpdateSuccess([0], 1);
        result.should.be.eql(false);
        done();
      });
    });
    describe('updated number of rows = expected number', () => {
      it('It should return true', (done) => {
        const result = helper.isSqlUpdateSuccess([1], 1);
        result.should.be.eql(true);
        done();
      });
    });
  });

  describe('isSqlInsertSuccess', () => {
    describe('result does not include valid primary key column', () => {
      it('It should return false', (done) => {
        var result = helper.isSqlInsertSuccess('error', 'uid');
        result.should.be.eql(false);

        result = helper.isSqlInsertSuccess({ key: 'value' }, 'uid');
        result.should.be.eql(false);

        done();
      });
    });
    describe('result includes valid primary key column', () => {
      it('It should return true', (done) => {
        const result = helper.isSqlInsertSuccess({ uid: 'uuid' }, 'uid');
        result.should.be.eql(true);
        done();
      });
    });
  });

});