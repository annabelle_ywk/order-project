const async = require('async');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const nock = require('nock');
const config = require('../../config');
const constants = require('../../constants');
const orderServices = require('../../services/orderServices');
const googleResponse = require('../test_data/google-response');

chai.use(chaiHttp);

describe('--------------- services/orderServices ---------------', () => {

  describe('getAllOrders', () => {
    var orders = [];

    beforeEach((done) => {
      dbconn.Order.destroy({ where: {}, truncate: true }).then(() => {
        async.map(
          [1111, 2222, 3333, 4444, 5555],
          function(d, callback) {
            dbconn.Order
            .create({ distance: d, status: constants.ORDER_STATUS.UNASSIGNED })
            .then(result => {
              callback(null, {
                id: result.id,
                distance: d,
                status: constants.ORDER_STATUS.UNASSIGNED
              });
            })
            .catch(err => {
              callback(null, null);
            });
          },
          function(err, result) {
            orders = result;
            done();
          }
        );
      });
    });

    describe('page = 1, limit = #orders', () => {
      it('It should return all orders in database', (done) => {
        orderServices.getAllOrders(1, orders.length, function(success, result) {
          success.should.eql(true);
          result.should.be.a('array');
          result.should.be.lengthOf(orders.length);
          for (var i = 0; i < orders.length; i++) {
            result[i].should.be.a('object');
            result[i].should.have.property('id').eql(orders[i].id);
            result[i].should.have.property('distance').eql(orders[i].distance);
            result[i].should.have.property('status').eql(orders[i].status);
          }
          done();
        });
      });
    });

    describe('page = 2, limit = 2', () => {
      it('It should return order #3 and #4 in database', (done) => {
        orderServices.getAllOrders(2, 2, function(success, result) {
          success.should.eql(true);
          result.should.be.a('array');
          result.should.be.lengthOf(2);
          for (var i = 0; i < 2; i++) {
            result[i].should.be.a('object');
            result[i].should.have.property('id').eql(orders[2+i].id);
            result[i].should.have.property('distance').eql(orders[2+i].distance);
            result[i].should.have.property('status').eql(orders[2+i].status);
          }
          done();
        });
      });
    });

    describe('page = #orders+1, limit = 1', () => {
      it('It should return empty array', (done) => {
        orderServices.getAllOrders(orders.length+1, 1, function(success, result) {
          success.should.eql(true);
          result.should.be.a('array');
          result.should.be.lengthOf(0);
          done();
        });
      });
    });

    describe('no pagination', () => {
      it('It should return all orders in database', (done) => {
        orderServices.getAllOrders(null, null, function(success, result) {
          success.should.eql(true);
          result.should.be.a('array');
          result.should.be.lengthOf(orders.length);
          for (var i = 0; i < orders.length; i++) {
            result[i].should.be.a('object');
            result[i].should.have.property('id').eql(orders[i].id);
            result[i].should.have.property('distance').eql(orders[i].distance);
            result[i].should.have.property('status').eql(orders[i].status);
          }
          done();
        });
      });
    });
  });

  describe('getOrderById', () => {
    var orders = [];

    beforeEach((done) => {
      dbconn.Order.destroy({ where: {}, truncate: true }).then(() => {
        async.map(
          [1111, 2222],
          function(d, callback) {
            dbconn.Order
            .create({ distance: d, status: constants.ORDER_STATUS.UNASSIGNED })
            .then(result => {
              callback(null, {
                id: result.id,
                distance: d,
                status: constants.ORDER_STATUS.UNASSIGNED
              });
            })
            .catch(err => {
              callback(null, null);
            });
          },
          function(err, result) {
            orders = result;
            done();
          }
        );
      });
    });

    describe('order of given id exists', () => {
      it('It should return one order', (done) => {
        orderServices.getOrderById(orders[1].id, function(success, result) {
          success.should.eql(true);
          result.should.be.a('object');
          result.should.have.property('id').eql(orders[1].id);
          result.should.have.property('distance').eql(orders[1].distance);
          result.should.have.property('status').eql(orders[1].status);
          done();
        });
      });
    });

    describe('order of given id does not exist', () => {
      it('It should return null', (done) => {
        orderServices.getOrderById('123', function(success, result) {
          success.should.eql(true);
          chai.expect(result).to.be.null;
          done();
        });
      });
    });
  });

  describe('createOrder', () => {
    var orders = [];

    beforeEach((done) => {
      dbconn.Order.destroy({ where: {}, truncate: true })
      .then(() => done());
    });

    describe('valid record', () => {
      it('It should return id of order saved in database', (done) => {
        orderServices.createOrder(1, function(success, result) {
          success.should.eql(true);
          result.should.be.a('string');
          done();
        });
      });
    });

    describe('invalid record', () => {
      it('It should return success = false', (done) => {
        orderServices.createOrder('abcdef', function(success, result) {
          success.should.eql(false);
          done();
        });
      });
    });
  });

  describe('takeOrder', () => {
    var orders = [];

    beforeEach((done) => {
      dbconn.Order.destroy({ where: {}, truncate: true }).then(() => {
        async.map(
          [1111, 2222, 3333, 4444, 5555],
          function(d, callback) {
            dbconn.Order
            .create({ distance: d, status: constants.ORDER_STATUS.UNASSIGNED })
            .then(result => {
              callback(null, {
                id: result.id,
                distance: d,
                status: constants.ORDER_STATUS.UNASSIGNED
              });
            })
            .catch(err => {
              callback(null, null);
            });
          },
          function(err, result) {
            orders = result;
            done();
          }
        );
      });
    });

    describe('try taking order in database', () => {
      it('It should return success = true', (done) => {
        orderServices.takeOrder(orders[0].id, function(success, result) {
          success.should.eql(true);
          done();
        });
      });
    });

    describe('try taking order not in database', () => {
      it('It should return success = false', (done) => {
        orderServices.takeOrder('1234', function(success, result) {
          success.should.eql(false);
          chai.expect(result).to.be.null;
          done();
        });
      });
    });
  });

  describe('obtainDistance', () => {
    const coordinatePairs = {
      success: { origin: ['1', '1'], destination: ['0', '0'] },
      failZeroResult: { origin: ['1', '1'], destination: ['2', '2'] },
      failNotFound: { origin: ['1', '1'], destination: ['3', '3'] },
      failInvalidRequest: { origin: ['1', '1'], destination: ['4', '4'] },
      failEmptyResponse: { origin: ['1', '1'], destination: ['5', '5'] },
      failHttpError: { origin: ['1', '1'], destination: ['6', '6'] } 
    }
    const anotherKey = 'anotherKey';

    beforeEach(() => {
      //Google Map API - success response
      nock('https://maps.googleapis.com')
      .get('/maps/api/distancematrix/json')
      .query({
        units: 'metric',
        mode: 'driving',
        origins: coordinatePairs.success.origin.join(','),
        destinations: coordinatePairs.success.destination.join(','),
        key: config.GOOGLE_API_KEY
      })
      .reply(200, googleResponse.success);
  
      //Google Map API - zero_result response
      nock('https://maps.googleapis.com')
      .get('/maps/api/distancematrix/json')
      .query({
        units: 'metric',
        mode: 'driving',
        origins: coordinatePairs.failZeroResult.origin.join(','),
        destinations: coordinatePairs.failZeroResult.destination.join(','),
        key: config.GOOGLE_API_KEY
      })
      .reply(200, googleResponse.failZeroResult);
  
      //Google Map API - not_found response
      nock('https://maps.googleapis.com')
      .get('/maps/api/distancematrix/json')
      .query({
        units: 'metric',
        mode: 'driving',
        origins: coordinatePairs.failNotFound.origin.join(','),
        destinations: coordinatePairs.failNotFound.destination.join(','),
        key: config.GOOGLE_API_KEY
      })
      .reply(200, googleResponse.failNotFound);
  
      //Google Map API - invalid_key response
      nock('https://maps.googleapis.com')
      .get('/maps/api/distancematrix/json')
      .query({
        units: 'metric',
        mode: 'driving',
        origins: coordinatePairs.success.origin.join(','),
        destinations: coordinatePairs.success.destination.join(','),
        key: anotherKey
      })
      .reply(200, googleResponse.failInvalidKey);
  
      //Google Map API - invalid_request response
      nock('https://maps.googleapis.com')
      .get('/maps/api/distancematrix/json')
      .query({
        units: 'metric',
        mode: 'driving',
        origins: coordinatePairs.failInvalidRequest.origin.join(','),
        destinations: coordinatePairs.failInvalidRequest.destination.join(','),
        key: config.GOOGLE_API_KEY
      })
      .reply(200, googleResponse.failInvalidRequest);

      //Google Map API - empty response
      nock('https://maps.googleapis.com')
      .get('/maps/api/distancematrix/json')
      .query({
        units: 'metric',
        mode: 'driving',
        origins: coordinatePairs.failEmptyResponse.origin.join(','),
        destinations: coordinatePairs.failEmptyResponse.destination.join(','),
        key: config.GOOGLE_API_KEY
      })
      .reply(204);

      //Google Map API - http error 500
      nock('https://maps.googleapis.com')
      .get('/maps/api/distancematrix/json')
      .query({
        units: 'metric',
        mode: 'driving',
        origins: coordinatePairs.failHttpError.origin.join(','),
        destinations: coordinatePairs.failHttpError.destination.join(','),
        key: config.GOOGLE_API_KEY
      })
      .reply(500, { 'error': 'mock internal server error' });
    });

    describe('successfully obtain distance from Google', () => {
      it('It should return distance > 0', (done) => {
        orderServices.obtainDistance(
          coordinatePairs.success.origin[0],
          coordinatePairs.success.origin[1],
          coordinatePairs.success.destination[0],
          coordinatePairs.success.destination[1],
          function(distance) {
            distance.should.be.a('number');
            chai.expect(distance).to.be.above(0);
            done();
          }
        );
      });
    });

    describe('empty response from Google', () => {
      it('It should return null', (done) => {
        orderServices.obtainDistance(
          coordinatePairs.failEmptyResponse.origin[0],
          coordinatePairs.failEmptyResponse.origin[1],
          coordinatePairs.failEmptyResponse.destination[0],
          coordinatePairs.failEmptyResponse.destination[1],
          function(distance) {
            chai.expect(distance).to.be.null;
            done();
          }
        );
      });
    });

    describe('http error from Google', () => {
      it('It should return null', (done) => {
        orderServices.obtainDistance(
          coordinatePairs.failHttpError.origin[0],
          coordinatePairs.failHttpError.origin[1],
          coordinatePairs.failHttpError.destination[0],
          coordinatePairs.failHttpError.destination[1],
          function(distance) {
            chai.expect(distance).to.be.null;
            done();
          }
        );
      });
    });

    describe('invalid-request response from Google', () => {
      it('It should return null', (done) => {
        orderServices.obtainDistance(
          coordinatePairs.failInvalidRequest.origin[0],
          coordinatePairs.failInvalidRequest.origin[1],
          coordinatePairs.failInvalidRequest.destination[0],
          coordinatePairs.failInvalidRequest.destination[1],
          function(distance) {
            chai.expect(distance).to.be.null;
            done();
          }
        );
      });
    });

    describe('invalid-key response from Google', () => {
      it('It should return null', (done) => {
        const key = config.GOOGLE_API_KEY;
        config.GOOGLE_API_KEY = anotherKey;
        
        orderServices.obtainDistance(
          coordinatePairs.success.origin[0],
          coordinatePairs.success.origin[1],
          coordinatePairs.success.destination[0],
          coordinatePairs.success.destination[1],
          function(distance) {
            config.GOOGLE_API_KEY = key;
            chai.expect(distance).to.be.null;
            done();
          }
        );
      });
    });

    describe('zero-result response from Google', () => {
      it('It should return 0', (done) => {
        orderServices.obtainDistance(
          coordinatePairs.failZeroResult.origin[0],
          coordinatePairs.failZeroResult.origin[1],
          coordinatePairs.failZeroResult.destination[0],
          coordinatePairs.failZeroResult.destination[1],
          function(distance) {
            distance.should.be.a('number').eql(0);
            done();
          }
        );
      });
    });

    describe('not-found response from Google', () => {
      it('It should return null', (done) => {
        orderServices.obtainDistance(
          coordinatePairs.failNotFound.origin[0],
          coordinatePairs.failNotFound.origin[1],
          coordinatePairs.failNotFound.destination[0],
          coordinatePairs.failNotFound.destination[1],
          function(distance) {
            chai.expect(distance).to.be.null;
            done();
          }
        );
      });
    });
  });

});
