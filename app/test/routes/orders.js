const async = require('async');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const nock = require('nock');
const server = require('../../server');
const config = require('../../config');
const constants = require('../../constants');
const orderServices = require('../../services/orderServices');
const googleResponse = require('../test_data/google-response');

chai.use(chaiHttp);

describe('--------------- routes/orders ---------------', () => {

  describe('POST /orders', () => {
    const coordinatePairs = {
      success: { origin: ['22.492096', '114.138661'], destination: ['22.592096', '114.238661'] },
      failZeroResult: { origin: ['22.492096', '114.138661'], destination: ['22.282857', '114.158145'] },
      failHttpError: { origin: ['0', '0'], destination: ['1', '1'] }
    }

    beforeEach(() => {
      //Google Map API - success response
      nock('https://maps.googleapis.com')
      .get('/maps/api/distancematrix/json')
      .query({
        units: 'metric',
        mode: 'driving',
        origins: coordinatePairs.success.origin.join(','),
        destinations: coordinatePairs.success.destination.join(','),
        key: config.GOOGLE_API_KEY
      })
      .reply(200, googleResponse.success);

      //Google Map API - zero_result response
      nock('https://maps.googleapis.com')
      .get('/maps/api/distancematrix/json')
      .query({
        units: 'metric',
        mode: 'driving',
        origins: coordinatePairs.failZeroResult.origin.join(','),
        destinations: coordinatePairs.failZeroResult.destination.join(','),
        key: config.GOOGLE_API_KEY
      })
      .reply(200, googleResponse.failZeroResult);

      //Google Map API - http error 500
      nock('https://maps.googleapis.com')
      .get('/maps/api/distancematrix/json')
      .query({
        units: 'metric',
        mode: 'driving',
        origins: coordinatePairs.failHttpError.origin.join(','),
        destinations: coordinatePairs.failHttpError.destination.join(','),
        key: config.GOOGLE_API_KEY
      })
      .reply(500, { 'error': 'mock internal server error' });
    });

    describe('success', () => {
      it('It should obtain valid response from Google Map API, create and save the order to database', (done) => {
        chai.request(server)
        .post('/orders')
        .send(coordinatePairs.success)
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_SUCCESS);
          res.body.should.be.a('object');
          res.body.should.have.property('id');
          res.body.id.should.be.a('string');
          res.body.should.have.property('distance');
          res.body.distance.should.be.a('number');
          res.body.should.have.property('status').eql(constants.ORDER_STATUS.UNASSIGNED);
          done();
        });
      });
    });

    describe('failed - Google Map API returns zero_result response', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send(coordinatePairs.failZeroResult)
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.ZERO_DISTANCE);
          done();
        });
      });
    });

    describe('failed - Google Map API returns http status error', () => {
      it('It should return 500 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send(coordinatePairs.failHttpError)
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_INTERNAL_SERVER_ERROR);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.UNABLE_TO_GET_DISTANCE);
          done();
        });
      });
    });

    describe('failed - empty body', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send()
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_ORIGIN);
          done();
        });
      });
    });

    describe('failed - origin not array', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send({
          origin: '1, 1',
          destination: coordinatePairs.success.destination
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_ORIGIN);
          done();
        });
      });
    });

    describe('failed - destination not array', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send({
          origin: coordinatePairs.success.origin,
          destination: '1, 1'
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_DESTINATION);
          done();
        });
      });
    });

    describe('failed - origin does not pass double string check', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send({
          origin: ['1', 1],
          destination: coordinatePairs.success.destination
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_ORIGIN);
          done();
        });
      });
    });

    describe('failed - destination does not pass double string check', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send({
          origin: coordinatePairs.success.origin,
          destination: ['1', '1.2.3']
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_DESTINATION);
          done();
        });
      });
    });

    describe('failed - origin latitude smaller than -90', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send({
          origin: ['-91', '1'],
          destination: coordinatePairs.success.destination
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_ORIGIN);
          done();
        });
      });
    });

    describe('failed - origin latitude larger than 90', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send({
          origin: ['91', '1'],
          destination: coordinatePairs.success.destination
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_ORIGIN);
          done();
        });
      });
    });

    describe('failed - origin longitude smaller than -180', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send({
          origin: ['1', '-181'],
          destination: coordinatePairs.success.destination
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_ORIGIN);
          done();
        });
      });
    });

    describe('failed - origin longitude larger than 180', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .post('/orders')
        .send({
          origin: ['1', '181'],
          destination: coordinatePairs.success.destination
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_ORIGIN);
          done();
        });
      });
    });
  });

  describe('GET /orders?page=:page&limit=:limit', () => {
    var orders = [];

    beforeEach((done) => {
      dbconn.Order.destroy({ where: {}, truncate: true }).then(() => {
        async.map(
          [1111, 2222, 3333, 4444, 5555],
          function(d, callback) {
            dbconn.Order
            .create({ distance: d, status: constants.ORDER_STATUS.UNASSIGNED })
            .then(result => {
              callback(null, {
                id: result.id,
                distance: d,
                status: constants.ORDER_STATUS.UNASSIGNED
              });
            })
            .catch(err => {
              callback(null, null);
            });
          },
          function(err, result) {
            orders = result;
            done();
          }
        );
      });
    });

    describe('success', () => {
      it('It should retrieve all orders in database', (done) => {
        chai.request(server)
        .get('/orders?page=1&limit='+orders.length)
        .send()
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_SUCCESS);
          res.body.should.be.a('array');
          res.body.should.be.lengthOf(orders.length);
          for (var i = 0; i < orders.length; i++) {
            res.body[i].should.be.a('object');
            res.body[i].should.have.property('id').eql(orders[i].id);
            res.body[i].should.have.property('distance').eql(orders[i].distance);
            res.body[i].should.have.property('status').eql(orders[i].status);
          }
          done();
        });
      });
    });

    describe('failed - missing query parameter page', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .get('/orders?limit=1')
        .send()
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.GET.MISSING_REQUEST_QUERY);
          done();
        });
      });
    });

    describe('failed - missing query parameter page', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .get('/orders?page=0&limit=1')
        .send()
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.GET.INVALID_REQUEST_QUERY_PAGE);
          done();
        });
      });
    });

    describe('failed - missing query parameter limit', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .get('/orders?page=1&limit=0')
        .send()
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.GET.INVALID_REQUEST_QUERY_LIMIT);
          done();
        });
      });
    });
  });

  describe('PATCH /orders/:id', () => {
    var orders = [];

    beforeEach((done) => {
      dbconn.Order.destroy({ where: {}, truncate: true }).then(() => {
        async.map(
          [1111, 2222, 3333, 4444],
          function(d, callback) {
            dbconn.Order
            .create({ distance: d, status: constants.ORDER_STATUS.UNASSIGNED })
            .then(result => {
              callback(null, {
                id: result.id,
                distance: d,
                status: constants.ORDER_STATUS.UNASSIGNED
              });
            })
            .catch(err => {
              callback(null, null);
            });
          },
          function(err, result) {
            orders = result;
            done();
          }
        );
      });
    });

    describe('success - single attempt on order #1', () => {
      it('It should return 200 with result { "status": "SUCCESS" }', (done) => {
        chai.request(server)
        .patch('/orders/'+orders[0].id)
        .send({
          status: constants.ORDER_STATUS.TAKEN
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_SUCCESS);
          res.body.should.be.a('object');
          res.body.should.have.property('status').eql(constants.RESPONSE_MESSAGE.ORDER.TAKE.SUCCESS);
          done();
        });
      });
    });

    describe('success - single attempt on order #2 with id uppercased', () => {
      it('It should return 200 with result { "status": "SUCCESS" }', (done) => {
        chai.request(server)
        .patch('/orders/'+orders[1].id.toUpperCase())
        .send({
          status: constants.ORDER_STATUS.TAKEN
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_SUCCESS);
          res.body.should.be.a('object');
          res.body.should.have.property('status').eql(constants.RESPONSE_MESSAGE.ORDER.TAKE.SUCCESS);
          done();
        });
      });
    });

    describe('success - multiple attempts on order #3', () => {
      it('One result should return 200 with result { "status": "SUCCESS" }, others should return 400 with error message', (done) => {
        async.map([1,2,3,4,5,6,7,8,9,10],
          function(i, callback) {
            chai.request(server)
            .patch('/orders/'+orders[2].id)
            .send({
              status: constants.ORDER_STATUS.TAKEN
            })
            .end((err, res) => {
              callback(null, {
                status: res.status,
                body: res.body
              });
            });
          },
          function(err, result) {
            var successCount = 0;
            var successCounter = -1;
            for (var i = 0; i < 10; i++) {
              if (result[i].status == constants.HTTP_CODE_SUCCESS) {
                successCount++;
                successCounter = i;
              }
            }
            successCount.should.eql(1);
            successCounter.should.not.eql(-1);
            for (var i = 0; i < 10; i++) {
              if (successCounter == i) {
                result[i].status.should.eql(constants.HTTP_CODE_SUCCESS);
                result[i].body.should.be.a('object');
                result[i].body.should.have.property('status').eql(constants.RESPONSE_MESSAGE.ORDER.TAKE.SUCCESS);
              } else {
                result[i].status.should.eql(constants.HTTP_CODE_BAD_REQUEST);
                result[i].body.should.be.a('object');
                result[i].body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.TAKE.ALREADY_TAKEN);
              }
            }
            done();
          }
        );
      });
    });

    describe('failed - invalid id', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .patch('/orders/123')
        .send({
          status: constants.ORDER_STATUS.TAKEN
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.TAKE.INVALID_REQUEST_PARAM_ID);
          done();
        });
      });
    });

    describe('failed - empty body', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .patch('/orders/'+orders[3].id)
        .send()
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.TAKE.MISSING_REQUEST_BODY_STATUS);
          done();
        });
      });
    });

    describe('failed - status invalid', () => {
      it('It should return 400 with error message', (done) => {
        chai.request(server)
        .patch('/orders/'+orders[3].id)
        .send({
          status: 'COMPLETED'
        })
        .end((err, res) => {
          res.should.have.status(constants.HTTP_CODE_BAD_REQUEST);
          res.body.should.be.a('object');
          res.body.should.have.property('error').eql(constants.RESPONSE_MESSAGE.ORDER.TAKE.INVALID_REQUEST_BODY_STATUS);
          done();
        });
      });
    });
  });

});
