const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const server = require('../../server');
const constants = require('../../constants');

chai.use(chaiHttp);

describe('Path not found', () => {
  it('It should return 404', (done) => {
    chai.request(server)
    .get('/abc')
    .send()
    .end((err, res) => {
      res.should.have.status(constants.HTTP_CODE_NOT_FOUND);
      done();
    });
  });
});

describe('Health Check', () => {
  it('It should return 200', (done) => {
    chai.request(server)
    .get('/_health')
    .send()
    .end((err, res) => {
      res.should.have.status(constants.HTTP_CODE_SUCCESS);
      done();
    });
  });
});
