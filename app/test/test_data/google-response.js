module.exports = {
  success: {
    "destination_addresses": [
      "Exchange Square, Central, Hong Kong"
    ],
    "origin_addresses": [
      "Fanling Station, Fanling, Hong Kong"
    ],
    "rows": [
      {
        "elements": [
          {
            "distance": {
              "text": "35.2 km",
              "value": 35225
            },
            "duration": {
              "text": "34 mins",
              "value": 2025
            },
            "status": "OK"
          }
        ]
      }
    ],
    "status": "OK"
  },
  failZeroResult: {
    "destination_addresses": [
      "22.592096,114.238661"
    ],
    "origin_addresses": [
      "22.492096,114.138661"
    ],
    "rows": [
      {
        "elements": [
          {
            "status": "ZERO_RESULTS"
          }
        ]
      }
    ],
    "status": "OK"
  },
  failNotFound: {
    "destination_addresses": [
      ""
    ],
    "origin_addresses": [
      ""
    ],
    "rows": [
      {
        "elements": [
          {
            "status": "NOT_FOUND"
          }
        ]
      }
    ],
    "status": "OK"
  },
  failInvalidKey: {
    "destination_addresses": [],
    "error_message": "The provided API key is invalid.",
    "origin_addresses": [],
    "rows": [],
    "status": "REQUEST_DENIED"
  },
  failInvalidRequest: {
    "destination_addresses": [],
    "origin_addresses": [],
    "rows": [],
    "status": "INVALID_REQUEST"
  }
}
