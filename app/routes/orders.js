const express = require('express');
const router = express.Router();
const constants = require('../constants');
const helper = require('../util/helper');
const orderServices = require('../services/orderServices');

router.get('/', function(req, res) {
  if (req.query == null || req.query.page == null || req.query.limit == null) {
    helper.sendResponse(res)(constants.HTTP_CODE_BAD_REQUEST, { error: constants.RESPONSE_MESSAGE.ORDER.GET.MISSING_REQUEST_QUERY });
    return;
  }
  const page = req.query.page;
  const limit = req.query.limit;
  if (!helper.isPositiveInteger(page, 1, null)) {
    helper.sendResponse(res)(constants.HTTP_CODE_BAD_REQUEST, { error: constants.RESPONSE_MESSAGE.ORDER.GET.INVALID_REQUEST_QUERY_PAGE });
    return;
  }
  if (!helper.isPositiveInteger(limit, 1, null)) {
    helper.sendResponse(res)(constants.HTTP_CODE_BAD_REQUEST, { error: constants.RESPONSE_MESSAGE.ORDER.GET.INVALID_REQUEST_QUERY_LIMIT });
    return;
  }
  orderServices.getAllOrders(page, limit, function(success, result) {
    if (!success) {
      logger.error('Unable to perform order search in database', { error: result });
      helper.sendResponse(res)(constants.HTTP_CODE_INTERNAL_SERVER_ERROR, { error: constants.RESPONSE_MESSAGE.ORDER.GET.DB_ERROR });
      return;
    }
    helper.sendResponse(res)(constants.HTTP_CODE_SUCCESS, result);
  });
});

router.post('/', function(req, res) {
  const origin = req.body != null? req.body.origin : null;
  if (!helper.isArray(origin, 2, 2) ||
      !helper.isDouble(origin[0], constants.LATITUDE.MIN, true, constants.LATITUDE.MAX, true) ||
      !helper.isDouble(origin[1], constants.LONGITUDE.MIN, true, constants.LONGITUDE.MAX, true)
  ) {
    helper.sendResponse(res)(constants.HTTP_CODE_BAD_REQUEST, { error: constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_ORIGIN });
    return;
  }
  const destination = req.body.destination;
  if (!helper.isArray(destination, 2, 2) ||
      !helper.isDouble(destination[0], constants.LATITUDE.MIN, true, constants.LATITUDE.MAX, true) ||
      !helper.isDouble(destination[1], constants.LONGITUDE.MIN, true, constants.LONGITUDE.MAX, true)
  ) {
    helper.sendResponse(res)(constants.HTTP_CODE_BAD_REQUEST, { error: constants.RESPONSE_MESSAGE.ORDER.CREATE.INVALID_REQUEST_BODY_DESTINATION });
    return;
  }
  orderServices.obtainDistance(origin[0], origin[1], destination[0], destination[1], function(distance) {
    if (distance == null || distance < 0) {
      helper.sendResponse(res)(constants.HTTP_CODE_INTERNAL_SERVER_ERROR, { error: constants.RESPONSE_MESSAGE.ORDER.CREATE.UNABLE_TO_GET_DISTANCE });
      return;
    }
    if (distance == 0) {
      helper.sendResponse(res)(constants.HTTP_CODE_BAD_REQUEST, { error: constants.RESPONSE_MESSAGE.ORDER.CREATE.ZERO_DISTANCE });
      return;
    }
    orderServices.createOrder(distance, function(success, result) {
      if (!success) {
        logger.error('Unable to create order in database', { error: result });
        helper.sendResponse(res)(constants.HTTP_CODE_INTERNAL_SERVER_ERROR, { error: constants.RESPONSE_MESSAGE.ORDER.CREATE.DB_ERROR });
        return;
      }
      helper.sendResponse(res)(
        constants.HTTP_CODE_SUCCESS,
        {
          id: result,
          distance: distance,
          status: constants.ORDER_STATUS.UNASSIGNED
        }
      );
    });
  });
});

router.patch('/:id', function(req, res) {
  if (req.params == null || !helper.isUUID(req.params.id)) {
    helper.sendResponse(res)(constants.HTTP_CODE_BAD_REQUEST, { error: constants.RESPONSE_MESSAGE.ORDER.TAKE.INVALID_REQUEST_PARAM_ID });
    return;
  }
  const id = req.params.id.toLowerCase();
  if (req.body == null || req.body.status == null) {
    helper.sendResponse(res)(constants.HTTP_CODE_BAD_REQUEST, { error: constants.RESPONSE_MESSAGE.ORDER.TAKE.MISSING_REQUEST_BODY_STATUS });
    return;
  }
  if (req.body.status != constants.ORDER_STATUS.TAKEN) {
    helper.sendResponse(res)(constants.HTTP_CODE_BAD_REQUEST, { error: constants.RESPONSE_MESSAGE.ORDER.TAKE.INVALID_REQUEST_BODY_STATUS });
    return;
  }
  orderServices.getOrderById(id, function(success, result) {
    if (!success) {
      logger.error('Unable to perform order search in database', { error: result });
      helper.sendResponse(res)(constants.HTTP_CODE_INTERNAL_SERVER_ERROR, { error: constants.RESPONSE_MESSAGE.ORDER.TAKE.DB_ERROR });
      return;
    }
    orderServices.takeOrder(id, function(success, result) {
      if (!success) {
        if (result) {
          logger.error('Unable to update order status to ' + constants.ORDER_STATUS.TAKEN + ' in database', { error: result });
          helper.sendResponse(res)(constants.HTTP_CODE_INTERNAL_SERVER_ERROR, { error: constants.RESPONSE_MESSAGE.ORDER.TAKE.DB_ERROR });
          return;
        }
        helper.sendResponse(res)(constants.HTTP_CODE_BAD_REQUEST, { error: constants.RESPONSE_MESSAGE.ORDER.TAKE.ALREADY_TAKEN });
        return;
      }
      helper.sendResponse(res)(constants.HTTP_CODE_SUCCESS, { status: constants.RESPONSE_MESSAGE.ORDER.TAKE.SUCCESS });
    });
  });
});

module.exports = router;
