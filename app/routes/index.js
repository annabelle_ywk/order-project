const constants = require('../constants');
const express = require('express');
const router = express.Router();

router.get('/_health', function(req, res) {
  dbconn.sequelize.authenticate()
  .then(() => {
    helper.sendResponse(res)(constants.HTTP_CODE_SUCCESS, { message: 'OK' });
  })
  .catch(err => {
    logger.error('DB connection error', { error: err });
    helper.sendResponse(res)(constants.HTTP_CODE_INTERNAL_SERVER_ERROR, { message: 'DB connection error' });
  });
});

router.use('/orders', require('./orders'));

module.exports = router;
