module.exports = {
  HTTP_CODE_SUCCESS: 200,
  HTTP_CODE_BAD_REQUEST: 400,
  HTTP_CODE_UNAUTHORIZED: 400,
  HTTP_CODE_FORBIDDEN: 403,
  HTTP_CODE_NOT_FOUND: 404,
  HTTP_CODE_INTERNAL_SERVER_ERROR: 500,
  ORDER_STATUS: {
    UNASSIGNED: 'UNASSIGNED',
    TAKEN: 'TAKEN',
    COMPLETED: 'COMPLETED'
  },
  LATITUDE: {
    MIN: -90,
    MAX: 90
  },
  LONGITUDE: {
    MIN: -180,
    MAX: 180
  },
  GOOGLE_MAP_API_STATUS: {
    OK: 'OK',
    REQUEST_DENIED: 'REQUEST_DENIED',
    DISTANCE: {
      NOT_FOUND: 'NOT_FOUND',
      ZERO_RESULTS: 'ZERO_RESULTS',
      OK: 'OK'
    }
  },
  RESPONSE_MESSAGE: {
    ORDER: {
      CREATE: {
        INVALID_REQUEST_BODY_ORIGIN: 'Invalid origin input in request body',
        INVALID_REQUEST_BODY_DESTINATION: 'Invalid destination input in request body',
        UNABLE_TO_GET_DISTANCE: 'Unable to calculate distance for given origin and destination',
        ZERO_DISTANCE: 'Either 1. Distination cannot be reached from origin by driving, or 2. Driving distance between given origin and destination is zero',
        DB_ERROR: 'Unable to create order due to server error'
      },
      GET: {
        MISSING_REQUEST_QUERY: 'Missing query parameters <page> and/or <limit>',
        INVALID_REQUEST_QUERY_PAGE: 'Query parameter <page> should be positive integer starting from 1',
        INVALID_REQUEST_QUERY_LIMIT: 'Query parameter <limit> should be positive integer starting from 1',
        DB_ERROR: 'Unable to retrieve orders due to server error'
      },
      TAKE: {
        INVALID_REQUEST_PARAM_ID: 'ID should be in UUID format',
        MISSING_REQUEST_BODY_STATUS: 'Missing order status to be updated',
        INVALID_REQUEST_BODY_STATUS: 'Currently only support updating status to TAKEN',
        DB_ERROR: 'Unable to take order due to server error',
        ALREADY_TAKEN: 'Order was taken',
        SUCCESS: 'SUCCESS'
      }
    }
  }
};
