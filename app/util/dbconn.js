const Sequelize = require('sequelize');
const OrderModel = require('../models/order');
const config = require('../config');

const sequelizeConfig = (config.NODE_ENV == 'test'?
  {
    dialect: 'sqlite',
    storage: ':memory:',
    logging: false
  }
  :
  {
    host: config.DB_ENDPOINT,
    port: config.DB_PORT,
    username: config.DB_USERNAME,
    password: config.DB_PASSWORD,
    database: config.DB_SCHEMA,
    dialect: 'mysql',
    pool: {
      max: 10,
      min: 1,
      idle: 30 * 1000,
      acquire: 60 * 1000
    },
    omitNull: true,
    logging: msg => logger.debug(msg)
  }
);

const sequelize = new Sequelize(sequelizeConfig);
const Order = sequelize.define('order', OrderModel(Sequelize));
sequelize.sync();

module.exports = {
  Order: Order,
  Sequelize: Sequelize,
  sequelize: sequelize
};
