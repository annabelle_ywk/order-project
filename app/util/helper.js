module.exports = {
  logRequest: function(req) {
    req.inboundTimestamp = Date.now();
    logger.info('----- INBOUND Request -----', {
      method: req.method,
      url: req.originalUrl,
      request: {
        headers: req.headers,
        cookies: req.cookies,
        query: req.query,
        params: req.params,
        body: req.body
      }
    });
  },
  sendResponse: function(res) {
    return function(statusCode, respData) {
      logger.info('----- INBOUND Response -----', {
        method: res.req.method,
        url: res.req.originalUrl,
        responseTime: (Date.now() - res.req.inboundTimestamp),
        response: {
          status: statusCode,
          data: JSON.parse(JSON.stringify(respData))
        }
      });
      return res.status(statusCode).json(respData);
    }
  },
  logOutboundRequest: function(req) {
    logger.info('----- OUTBOUND Request -----', {
      method: req.method,
      url: req.url,
      request: {
        headers: req.headers,
        params: req.params,
        body: req.data
      }
    });
  },
  logOutboundResponseSuccess: function(res) {
    logger.info('----- OUTBOUND Response -----', {
      method: res.config.method.toUpperCase(),
      url: res.config.url,
      response: {
        status: res.status,
        headers: res.headers,
        body: JSON.parse(JSON.stringify(res.data))
      }
    });
  },
  logOutboundResponseError: function(res) {
    logger.info('----- OUTBOUND Response -----', {
      method: res.config.method.toUpperCase(),
      url: res.config.url,
      response: {
        error: res.stack
      }
    });
  },
  /* 
    Description: Verify if object is array, and optional check if array is of correct length
    Parameters:
      obj   - any; object to be verified
      min   - int; required minimum array length, null if there is no minimum
      max   - int; required maximum array length, null if there is no maximum
    Return: boolean
  */
  isArray: function(obj, min, max) {
    if (obj == null || !Array.isArray(obj)) {
      return false;
    }
    if (min != null && obj.length < min) {
      return false;
    }
    if (max != null && obj.length > max) {
      return false;
    }
    return true;
  },
  /* 
    Description: Verify if input string is double, and optional with boundary check
    Parameters:
      str     - string; string to be verified
      min     - number; null if no need to check lower boundary
      minInc  - boolean; useful if min != null; true if boundary includes min
      max     - number; null if no need to check upper boundary
      maxInc  - boolean; useful if max != null; true if boundary includes max
    Return: boolean
  */
  isDouble: function(str, min, minInc, max, maxInc) {
    if (str == null || typeof str !== 'string' || str.length == 0) {
      return false;
    }
    if (!(/^([\+\-]?)([0-9]{0,})($|\.[0-9]{1,}$)/.test(str))) {
      return false;
    }
    const num = Number(str);
    if (isNaN(num)) {
      return false;
    }
    if (min != null && ((minInc && num < min) || (!minInc && num <= min))) {
      return false;
    }
    if (max != null && ((maxInc && num > max) || (!maxInc && num >= max))) {
      return false;
    }
    return true;
  },
  /* 
    Description: Verify if input string is postive integer (without + sign), and optional with boundary check
    Parameters:
      str     - string; string to be verified
      min     - number; null if no need to check lower boundary (inclusive)
      max     - number; null if no need to check upper boundary (inclusive)
    Return: boolean
  */
  isPositiveInteger: function(str, min, max) {
    if (str == null || str.length == 0) {
      return false;
    }
    if (!(/^([0-9]{1,})$/.test(str))) {
      return false;
    }
    const num = Number(str);
    if (isNaN(num) || num <= 0) {
      return false;
    }
    if (min != null && num < min) {
      return false;
    }
    if (max != null && num > max) {
      return false;
    }
    return true;
  },
  /* 
    Description: Verify if input string is UUID (v1)
    Parameters:
      str     - string; string to be verified
    Return: boolean
  */
  isUUID: function(str) {
    if (str == null || str.length == 0) {
      return false;
    }
    if (!(/^[a-fA-F0-9]{8}\-[a-fA-F0-9]{4}\-[a-fA-F0-9]{4}\-[a-fA-F0-9]{4}\-[a-fA-F0-9]{12}$/.test(str))) {
      return false;
    }
    return true;
  },
  /* 
    Description: Verify if Sequelize update statement result is a successful result
    Remarks: Result should be array of single number = number of updated rows
    Parameters:
      result    - object; result from Sequelize update statement
      expected  - int; expected update successful result
    Return: boolean
  */
  isSqlUpdateSuccess: function(result, expected) {
    if (!this.isArray(result, 1, 1) || result[0] != expected) {
      return false;
    }
    return true;
  },
  /* 
    Description: Verify if Sequelize insert statement result is a successful result
    Remarks: Result should have property id (or the primary key column)
    Parameters:
      result    - object; result from Sequelize insert statement
      idColName - string; the name of table column ID
    Return: boolean
  */
  isSqlInsertSuccess: function(result, idColName) {
    if (result == null || result[idColName] == null) {
      return false;
    }
    return true;
  }
}
