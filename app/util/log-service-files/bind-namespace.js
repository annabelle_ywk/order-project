module.exports = {
  createMiddleware: function(options) {
    const namespace = options.namespace;
    return function(req, res, next) {
      namespace.bindEmitter(req);
      namespace.bindEmitter(res);
      namespace.run(function() {
        next();
      });
    };
  }
};
  