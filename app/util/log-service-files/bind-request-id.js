const uuid = require('node-uuid');

module.exports = {
  createMiddleware: function(options) {
    const namespace = options.namespace;
    return function(req, res, next) {
      req.id = uuid.v4();
      if (namespace.active)
        namespace.set('requestId', req.id);
      next();
    };
  }
};
