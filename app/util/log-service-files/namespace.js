const cls  = require('continuation-local-storage');
const uuid = require('node-uuid');

module.exports = {
  create: function(options = {}) {
    options.name = options.name || uuid.v4();
    return cls.createNamespace(options.name);
  }
};
