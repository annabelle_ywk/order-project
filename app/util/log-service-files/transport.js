const transport = {};

const TRANSPORT_DEFAULT_CONFIG = {
  json: true,
  timestamp: true,
  stringify: true,
  showLevel: true,
  handleExceptions: true,
  humanReadableUnhandledException: true,
  showRequestId: true
};

transport.extend = function(Base, defaults) {
  class Transport extends Base {
    constructor(options) {
      options = Object.assign({}, TRANSPORT_DEFAULT_CONFIG, defaults, options);
      super(options);
      this.showRequestId = options.showRequestId;
    }

    log(lvl, msg, meta, callback) {
      Object.assign(meta, {
        requestId: this.showRequestId && this.params ? this.params.requestId : undefined
      });
      return super.log(lvl, msg, meta, callback);
    }
  }

  return Transport;
};

module.exports = transport;
