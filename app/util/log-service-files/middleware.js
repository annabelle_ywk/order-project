const _ = require('lodash');
const bindNamespace = require('./bind-namespace');
const bindRequestId = require('./bind-request-id');

module.exports = {
  create: function(options) {
    return {
      bindNamespace: bindNamespace.createMiddleware(_.pick(options, 'namespace')),
      bindRequestId: bindRequestId.createMiddleware(_.pick(options, 'namespace'))
    };
  }
};
