const winston   = require('winston');
const transport = require('./transport');

const LoggerConsole = transport.extend(winston.transports.Console);

class Logger extends winston.Logger {
  constructor(options) {
    super(Object.assign(
      {},
      {
        levels: {
          error: 0,
          warn: 1,
          info: 2,
          debug: 3,
          silly: 4
        },
        transports: [new LoggerConsole()]
      },
      options
    ));
    this.namespace = options.namespace;
  }

  log() {
    this._names.forEach(name => {
      const params = this.transports[name].params = this.transports[name].params || {};
      params.requestId = this.namespace && this.namespace.get('requestId');
    });
    return super.log.apply(this, arguments);
  }
}

module.exports = Logger;
