const _ = require('lodash');
const Logger = require('./log-service-files/Logger');
const middleware = require('./log-service-files/middleware');
const namespace = require('./log-service-files/namespace');

class LogService {
  constructor(options = {}) {
    this.namespace = namespace.create({ name: options.namespace });
    this.middlewares = middleware.create({ namespace: this.namespace });
    this.logger = (options.logger && !_.isPlainObject(options.logger))?
      options.logger : new Logger(Object.assign({}, options.logger, { namespace: this.namespace }));
  }
}

module.exports = LogService;
