module.exports = {
  TZ: process.env.TZ || 'Asia/Hong_Kong',
  LOG_LEVEL: process.env.LOG_LEVEL || 'debug',
  NODE_ENV: process.env.NODE_ENV || 'localhost',
  PORT: process.env.PORT || 8080,
  DB_ENDPOINT: process.env.DB_ENDPOINT,
  DB_PORT: process.env.DB_PORT || 3306,
  DB_USERNAME: process.env.DB_USERNAME,
  DB_PASSWORD: process.env.DB_PASSWORD,
  DB_SCHEMA: process.env.DB_SCHEMA,
  GOOGLE_API_KEY: process.env.GOOGLE_API_KEY || 'key',
  GOOGLE_MAP_API_URL: process.env.GOOGLE_MAP_API_URL || 'https://maps.googleapis.com/maps/api/distancematrix/json',
  CORS_ALLOW_ORIGIN: process.env.CORS_ALLOW_ORIGIN || '*',
  CORS_ALLOW_METHODS: process.env.CORS_ALLOW_METHODS || 'GET,PUT,PATCH,POST,DELETE',
  CORS_ALLOW_HEADERS: process.env.CORS_ALLOW_HEADERS || 'Content-Type,Authorization'
};
